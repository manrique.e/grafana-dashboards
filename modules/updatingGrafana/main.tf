module "extractGrafanaPasswd" {
    source = "./extractGrafanaPasswd"
}
resource "null_resource" "printToken" {
 
    provisioner "local-exec" {
        command = "echo '${module.extractGrafanaPasswd.grafanaPassword}' > grafanapass.txt"
    }    

}


module "tokenCreation" {
    source = "./token-creation"

    url = var.url
    pwd = urlencode("${module.extractGrafanaPasswd.grafanaPassword}")
    
}



module "grafanaDashboards" {
    source = "./grafana-dashboards"

    url = var.url
    token = module.tokenCreation.token

}


