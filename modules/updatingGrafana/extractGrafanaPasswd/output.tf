output "grafanaPassword" {
    value = data.kubernetes_secret.grafanaUser.data["password"]
}