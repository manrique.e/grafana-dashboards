provider "kubernetes" {
    config_path = "${path.module}/files/kube-config.yaml"

}

data "kubernetes_secret" "grafanaUser" {
    metadata {
    name = "grafanauser"
    namespace = "grafana"
  }
}
