#!/bin/bash
set -e
echo "here-pre"

eval "$(jq -r '@sh "URL=\(.url) PWD=\(.pwd)"')"

echo "HERE - ${PWD} - ${URL}"
curl -X POST -H "Content-Type: application/json" -d '{"name":"apikeycurl", "role": "Admin", "secondsToLive": 60}' https://admin:${PWD}@${URL}/api/auth/keys --insecure | jq '.key'
