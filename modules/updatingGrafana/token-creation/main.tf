data "external" "curl" {
    program = ["${path.module}/scripts/createToken.sh"]

    query = {
        url = var.url
        pwd = var.pwd
    }

}