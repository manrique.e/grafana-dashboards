provider "grafana" {
    url = "https://${var.url}"
    auth = var.token
}

resource "grafana_dashboard" "k8sClusterSummary" {
    config_json = file("${path.module}/sources/K8s-Cluster-Summary.json")

}

resource "grafana_dashboard" "kubernetesAppMetrics" {
    config_json = file("${path.module}/sources/kubernetes-app-metrics.json")
}